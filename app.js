const express = require('express')
const app = express()
const port = 3000
const sequelize = require('./src/database/config')

app.get('/', (req, res) => {
  res.send('Hello World!')
})

app.listen(port, () => {
  console.log('application écoutant sur le port ' + port);
});